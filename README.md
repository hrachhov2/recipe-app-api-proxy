# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environemtn Variables

* `LISTEN_PORT` = Port to listen on (default: `8000`)
* `APP_HOST` = Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` = Port of the app to forwrd requets to (deefalt: `9000`)